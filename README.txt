"""
# Author -> Adrian Elinow
# Distributed Interactable Stochastic Multi Agent Simulae
#   (DISMAS)
# BSD 3-Clause License
# Copyright (c) 2018, Adrian Elinow
# All rights reserved.
"""

To start DISMAS server:

    ./startsimulaeserver.sh

OR

    python NJIN.py -c "./node_config.json"