import curses, time, sys
"""
# Author -> Adrian Elinow
# Distributed Interactable Stochastic Multi Agent Simulae
#   (DISMAS)
# BSD 3-Clause License
# Copyright (c) 2018, Adrian Elinow
# All rights reserved.
"""

class TerminalApp(): # See FIXES in Development.txt
    def __init__(self):
        print('Starting Terminal App...')   
        self.LOG = []
        self.VIEW = []

    def clear_view(self):
        pass
        #self.viewwin.clear()

    def show(self, data):
        print('term > '+data)
        #self.viewwin.addstr(data)

    def clear_log(self):
        pass
        #self.logwin.clear()

    def log(self, msg):
        print('term > '+msg)
        #self.logwin.addstr(msg)

    def start(self, scr):
        self.scr = scr
        self.scr.clear()
        self.scr.box()
        halfpt = int(curses.LINES/2)
        self.viewwin = self.scr.subwin(halfpt-2, curses.COLS-2, 2 ,2)
        self.logwin = self.scr.subwin(halfpt-2, curses.COLS-2 ,halfpt, 2)

        while True:
            pass

# This file is runnable. Implemented solely intented for testing purposes.

def main():
    term = TerminalApp()

if __name__ == '__main__':
    main()