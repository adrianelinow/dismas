import json, time
from copy import deepcopy as DC
from AStar import AStar

"""
# Author -> Adrian Elinow
# Distributed Interactable Stochastic Multi Agent Simulae
#   (DISMAS)
# BSD 3-Clause License
# Copyright (c) 2018, Adrian Elinow
# All rights reserved.
"""

class Simulae():
    def __init__(self, worldfile):
        ''' Given worldfile is JSON file.
        assigns json data to object variables type
            SimulaeObjects | Regions | Areas
        '''
        with open(worldfile, 'r+') as wf:
            data = json.loads(wf.read())
            self.areas = self.ar_list(data['areas'])
            self.simulaeobjects = self.so_list(data['simulaeobjects'])

    def ar_list(self, ls):
        ''' Builds list of Areas from JSON array
        '''
        arlist = []
        for e in ls:
            arlist.append(Area(e))
        return arlist

    def so_list(self, ls):
        ''' Build list of SimObjs from JSON array
        '''
        solist = []
        for e in ls:
            solist.append(SimulaeObject(e['references'],e['attributes'],e['assembly'],e['metadata'],e['memory']))
        return solist

    def stop(self):
        ''' Currently Superficial
        Will save current state of world as a .json file
        '''
        print('Halting Simulae Environment')
        print('WARNING => World save functionality not yet implemented!')

    def start(self, queue, terminal):
        ''' Starts a tick Sequence
        passed a queue instance for list of operations to do
        terminal for displaying output
        '''
        print('Starting Simulae')
        while True:
            print('??')
            # FIXES [ 2 ] ... See FIXES section of Development.txt
            for simobj in self.simulaeobjects:
                simobj.action(self)
            print('???')

            # FIXES [ 1 ] ... See FIXES section of Development.txt
            rep = self.report()
            terminal.clear_view()
            for area in rep:
                reg = '\n'
                for row in area.data:
                    rowlog = ''
                    for e in row:
                        rowlog+=('['+e+']')
                    reg += rowlog+'\n'
                terminal.show(reg)
            time.sleep(1)

    def report(self):
        ''' Returns current area, with SimulaeObject overlay
        '''
        rep = []
        for n in range(len(self.areas)):
            rep.append(DC(self.areas[n])) #append copies of all areas to 'rep'
        for simobj in self.simulaeobjects:
            sc = simobj.metadata['position']
            # overlays the 'icon' of each simobj onto the copied maps of all areas
            rep[int(simobj.metadata['area'])].data[sc[0]][sc[1]] = simobj.metadata['iconchar']
        return rep

class Area():
    def __init__(self, jsonobj):
        self.data = jsonobj

    def __str__(self):
        ''' Returns string version of Map
        '''
        out = ''
        for Y in self.data:
            for X in Y:
                out += str(X)
            out += '|'
        return out

class SimulaeObject():

    def __init__(self, references, attributes, assembly, metadata, memory):
        ''' Intialize SimulaeObject instance from Dictionaries
        '''
        self.references = references
        self.attributes = attributes
        self.assembly = assembly
        self.metadata = metadata
        self.memory = memory

    def action(self, region): # FIXES [ 2 ] ... See FIXES section of Development.txt  # [+]
        return
        ''' Tells SimulaeObject to act according to 
                its simple AI written in this function
        Called once per tick
        '''
        # THIS FUNCTION SHOULD BE OVERWRITTEN
        #       WHEN WRITING CUSTOM AI
        if self.memory['path'] == []:
            dest = (3,9) # sample destination
            self.memory['path'] = AStar(region.areas[self.metadata['area']], self.metadata['position'], dest)
        self.movedir(self.dir_to(self.memory['path'][0]))
        self.memory['path'].pop(0)

    def jsonify(self):
        ''' For converting SimulaeObject to JSON
                for use when saving (NOT IMPLEMENTED)
        '''
        return json.dumps(self.__dict__)

        # NONE OF THE BELOW METHODS ARE CURRENTLY IMPLEMENTED #

    def movetoarea(self):
        ''' For use when moving to position in a
                different area
        (THIS FUNCTION NOT YET IMPLEMENTED)
        '''
        pass

    def dir_to(self, step):
        ''' Given a position, will give direction towards
                the position.
        Can be recurringly used as simple pathfinder, but
            is limited, and liable to get stuck if obstacles
            lie in the way
        '''
        coords = self.metadata['position']
        return (step[0] - coords[0], step[1] - coords[1])

    def movedir(self, step):
        ''' The only function that allows the SimulaeObject 
                to move. 
        '''
        coords = self.metadata['position']
        self.metadata['position'] = (coords[0] + step[0], coords[1] + step[1])
