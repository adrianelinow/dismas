import socket, sys, multiprocessing as MP

"""
# Author -> Adrian Elinow
# Distributed Interactable Stochastic Multi Agent Simulae
#   (DISMAS)
# BSD 3-Clause License
# Copyright (c) 2018, Adrian Elinow
# All rights reserved.
"""

class TCPServer():
    def __init__(self, host, port, nodes=[]):
        # keep track of hostname and port number
        self.host = host
        self.port = port
        # Default block size of messages
        self.BLOCK_SIZE = 2048
        # For keeping track of open connections
        self.connections = []
        # List of Partner Nodes
        self.nodes = nodes
        ''' Initialize socket as inet4/TCP socket
            Binds to socket to host,port
            Raises error if failure
        '''
        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.bind((self.host, self.port))
        except Exception as e:
            print(e)
            sys.exit()

    def get_partner_addrs(self): # UNIMPLEMENTED
        ''' Returns dict of addresses of other NJIN Nodes
        TODO -> have list of partners or FIND them.
        
        Each server has copy of list of POSSIBLE live IPs

        '''
        pass

    def stop(self):
        ''' Stop function for soft-halting TCPServer instance
        Closes and re-integrates connection processes
        Closes socket
        '''
        for conn in self.connections:
            conn.terminate()
            conn.join()
        if self.socket:
            self.socket.close()

    def start(self, queue, terminal):
        ''' Start function to initiating TCPServer instance
        Begins listening
        Loops through accepting new connections
            Creates new connection subprocess
            adds them to connections list
            Starts connection subprocess with 'handle' function
        '''
        self.socket.listen(8)
        while True:
            conn, addr = self.socket.accept()
            terminal.log('New Connection ->'+str(addr))
            proc = MP.Process(target=self.handle, args=(conn, addr, terminal))
            proc.daemon = True
            proc.start()
            self.connections.append(proc)

    def handle(self, conn, addr, terminal): # FIXES [ 1 ] ... See FIXES in Development.txt  # [+]
        ''' Handle function for dealing with messages from connections
        Recieves message, decodes with utf-8
        Validates message using Cleansing Protocol
        If applicable, adds to NJIN queue
            Waits for NJIN response
            Replies with evaluation of message (encoded utf-8)
        Closes connection on client disconnect
        '''
        try:
            while True:
                data = conn.recv(self.BLOCK_SIZE).decode('utf-8')
                if data == '':
                    terminal.log('[CLD] Client Disconnect ['+str(addr)+']') #fixflag
                    conn.close()
                    break
                reply = terminal.log(('['+str(addr[0])+'] >>> '+data)) #fixflag
                conn.sendall(str('Recieved').encode('utf-8'))
        except Exception as e:
            print('[ERR] Problem handling connection/request',str(e)) #fixflag
        finally:
            conn.close()
