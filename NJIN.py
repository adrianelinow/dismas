from Simulae import *
from Server import TCPServer
from TerminalInterface import TerminalApp
from curses import wrapper
import multiprocessing as MP
import sys, os, time, getopt

"""
# Author -> Adrian Elinow
# Distributed Interactable Stochastic Multi Agent Simulae
#   (DISMAS)
# BSD 3-Clause License
# Copyright (c) 2018, Adrian Elinow
# All rights reserved.
"""
class SimulaeServer(): 
    def __init__(self, hostname, port, simulaefile, nodes):
        ''' INIT
        Creates TCP Server from assigned hostname and port number
            Raises Error if bind failure
        Creates Simulae Environment from given worldfile
            Raises Error if file parse error/initialization error
        Creates empty Operations Queue
        '''
        self.host = hostname # keep track of hostname
        self.port = port # keep track of port
        ''' Queue of operations from client-side connection
        Evaluated at the start of each Simulae Tick     
        '''
        self.queue = []
        ## INITIALIZE AND BIND TCP SERVER ##
        try:
            print('Init TCP/IP Server')
            self.server = TCPServer(hostname, port, nodes)
        except Exception as e:
            print(e)
            sys.exit
        ## INITIALIZE SIMULAE MODEL ##
        print('Init Simulae Environment')
        try:
            self.simulae = Simulae(simulaefile)
        except Exception as e:
            print(e)
            sys.exit()
        # Start Terminal Display within Wrapper
        print('INIT and Start Display\n###')
        try:
            #wrapper_basic(self.display)
            self.term = TerminalApp() # See FIXES in Development.txt
        except Exception as e:
            print(e)
            sys.exit()

    def display(self): # See FIXES in Development.txt
        self.term = TerminalApp()
        self.term.display()

    def stop(self):
        ''' Stop function for soft-halting SimulaeServer
        Calls stop of TCPServer and Simulae Environment processes
        '''
        self.tcpserver_process.terminate()
        self.tcpserver_process.join()
        self.server.stop()
        print("Halted TCP-Server")
        self.simulae_process.terminate()
        self.simulae_process.join()
        self.simulae.stop()
        print("Halted Simulae Environment")


    def start(self):
        ''' Start function for beginning SimulaeServer
        Creates TCPServer Process and Simulae Environment processes
        '''
        self.tcpserver_process = MP.Process(target=self.server.start, args=(self.queue,self.term,))
        self.simulae_process = MP.Process(target=self.simulae.start, args=(self.queue,self.term,))

        try:
            self.tcpserver_process.start()
            self.simulae_process.start()
            #wrapper(self.term.start)
            while True:
                # This traps NJIN, keeps program running
                #   Needed since simulae_process and tcpserver_process
                #   are separate processes...
                pass

        except Exception as e:
            print(e)
            sys.exit()
        finally:
            self.stop()

def main(*args):
    ''' Main function, starts SimulaeServer
    Parses sys.argv into host, port, worldfile arguments
        for starting SimulaeServer with custom args
    '''
    CONFIG = {}
    try:
        opts, args = getopt.getopt(sys.argv[1:],'c:')
        for opt, arg in opts:
            if opt == '-c': 
                CONFIG = json.loads(open(arg, 'r').read())
    except getopt.GetoptError as ge:
        print(ge)
        sys.exit()
    print(CONFIG)
    if not CONFIG or CONFIG is {}:
        raise Exception
    try:
        # Initialize and Start SimulaeServer
        simserv = SimulaeServer(CONFIG['host'],CONFIG['port'], CONFIG['source file'], CONFIG['nodes'])
        simserv.start()
    except KeyboardInterrupt as ke:
        sys.exit()
    finally:
        print('[END PROGRAM]')

if __name__ == '__main__':
    main()