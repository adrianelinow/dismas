import math

"""
# Author -> Adrian Elinow
# Distributed Interactable Stochastic Multi Agent Simulae
#   (DISMAS)
# BSD 3-Clause License
# Copyright (c) 2018, Adrian Elinow
# All rights reserved.
"""


class Tile(): # Solely utilized by the A* algorithm
	def __init__(self, x, y):
		self.pos = (x,y)
		self.passable = True
		self.prev = 0
		self.g = 0
		self.f = 0
		self.h = 0
		self.vh = 0

def visDist(a, b):
	return math.sqrt(math.pow(a[0]-b[0],2)+math.pow(a[1]-b[1],2))

def heuristic(a, b):
	return abs(a[0] - b[0]) + abs(a[1] - b[1])

def getNeighbors(tile, grid, neighbors, area):
	''' Retrieves passable tiles adjacent to [tile] 
			Does not count walls
	'''
	valids = []
	for i in neighbors:
		if tile.pos[0] + i[0] > 0 and tile.pos[0] + i[0] < len(grid[tile.pos[1]]) \
			and tile.pos[1] + i[1] > 0 and tile.pos[1] + i[1] < len(grid):
			t = Tile(tile.pos[0] + i[0],tile.pos[1] + i[1])
			if not area.data[t.pos[0]][t.pos[1]] == '#':
				valids.append(t)
	return valids


def AStar(data, startpos, endpos):
	''' A* is a pathfinding algorithm. 
	This A* implementation is designed to be compatable
		with objects and world models utilized in DISMAS

	NOTE: Requires optimization
	'''

	area = data
	neighbors = [(1,0),(0,1),(-1,0),(0,-1)]
	opened = [Tile(startpos[0], startpos[1])]
	last = opened[0]
	closed = []
	path = {}
	grid = []
	for Y in range(len(area.data)):
		row = []
		for X in range(len(area.data[Y])):
			row.append( Tile(X, Y) )
		grid.append(row)

	while len(opened) > 0:
		best = 0
		for i in range(len(opened)):
			if opened[i].f < opened[best].f:
				best = i
		if opened[i].g == opened[best].g and opened[i].vh < opened[best].vh:
			best = i
		current = opened[best]
		last = current

		if opened[best].pos == endpos:
			return lookback(path, current)[::-1] # reverses output of 'lookback' Method

		opened.remove(current)
		closed.append(current)

		curr_n = getNeighbors(current, grid, neighbors, area, )
		for n in curr_n:
			if n not in closed:
				tg = current.g + heuristic(n.pos, current.pos)
				if n not in opened:
					opened.append(n)
				elif tg >= n.g:
					continue
				path[n] = current
				n.g = tg
				n.h = heuristic(n.pos, endpos)
				n.vh = visDist(n.pos, endpos)
				n.f = n.g + n.h
				n.prev = current

def lookback(path, current):
	total = [current.pos]
	while current in path.keys():
		current = path[current]
		total.append(current.pos)
	return total



